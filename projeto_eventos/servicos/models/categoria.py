#!/usr/bin/env python
# -*- coding:utf-8 -*-
from django.db.models.base import ModelBase
from django.db.models.fields import CharField, IntegerField

class Categoria(ModelBase):

    nome = CharField(u"Nome", max_length=100, unique=True)
    ordem = IntegerField(u"Ordem", default=0, help_text=u"ordene como se fosse uma fila. Números menores tem prioridade")


    class Meta:
        verbose_name=u"Categoria"
        verbose_name_plural=u"Categorias"
        ordering = ["ordem"]
        app_label = "servicos"


    def __unicode__(self):
        return self.nome
